package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func romanToInt(roman string) int {
	romanMap := map[string]int{
		"M":  1000,
		"CM": 900,
		"D":  500,
		"CD": 400,
		"C":  100,
		"XC": 90,
		"L":  50,
		"XL": 40,
		"X":  10,
		"IX": 9,
		"V":  5,
		"IV": 4,
		"I":  1,
	}

	result := 0
	i := 0
	for i < len(roman) {
		for k, v := range romanMap {
			if strings.HasPrefix(roman[i:], k) {
				result += v
				i += len(k)
				break
			}
		}
	}
	return result
}

func intToRoman(num int) string {
	romanMap := []struct {
		value  int
		symbol string
	}{
		{1000, "M"},
		{900, "CM"},
		{500, "D"},
		{400, "CD"},
		{100, "C"},
		{90, "XC"},
		{50, "L"},
		{40, "XL"},
		{10, "X"},
		{9, "IX"},
		{5, "V"},
		{4, "IV"},
		{1, "I"},
	}

	result := ""
	for i := 0; i < len(romanMap); i++ {
		for num >= romanMap[i].value {
			result += romanMap[i].symbol
			num -= romanMap[i].value
		}
	}
	return result
}

func isRoman(num string) bool {
	for _, r := range num {
		if !strings.Contains("IVXLCDM", string(r)) {
			return false
		}
	}
	return true
}

func validateRomanInput(roman string) bool {
	if romanToInt(roman) > 10 {
		return false
	}
	for _, invalid := range []string{"IIII", "VV", "XXXX", "LL", "CC", "DD", "MMMM"} {
		if strings.Contains(roman, invalid) {
			return false
		}
	}
	return true
}

func main() {
	reader := bufio.NewReader(os.Stdin)

	for {
		fmt.Print("Введите число или математическую операцию: ")
		input, _ := reader.ReadString('\n')
		input = strings.TrimSpace(input)

		parts := strings.Split(input, " ")

		if len(parts) == 3 {
			if isRoman(parts[0]) && isRoman(parts[2]) {
				if !validateRomanInput(parts[0]) || !validateRomanInput(parts[2]) {
					if romanToInt(parts[0]) > 10 || romanToInt(parts[2]) > 10 {
						panic("Ошибка: Римские числа должны быть не больше X")
					} else {
						panic("Ошибка: Неверный формат римского числа")
					}
				}
				num1 := romanToInt(parts[0])
				num2 := romanToInt(parts[2])

				switch parts[1] {
				case "+":
					fmt.Println(intToRoman(num1 + num2))
				case "-":
					if num1-num2 <= 0 {
						panic("Ошибка: Результат не может быть отрицательным или 0")
					} else {
						fmt.Println(intToRoman(num1 - num2))
					}
				case "*":
					fmt.Println(intToRoman(num1 * num2))
				case "/":
					if num2 == 0 {
						panic("Ошибка: Деление на 0 невозможно")
					} else {
						result := num1 / num2
						if result == 0 {
							panic("Ошибка: Результат деления римских чисел не может быть 0")
						} else {
							fmt.Println(intToRoman(result))
						}
					}
				default:
					panic("Ошибка: Неверная математическая операция")
				}

			} else if !isRoman(parts[0]) && !isRoman(parts[2]) {
				num1, err := strconv.Atoi(parts[0])
				if err != nil {
					panic("Ошибка: Неверный формат числа")
				}
				num2, err := strconv.Atoi(parts[2])
				if err != nil {
					panic("Ошибка: Неверный формат числа")
				}

				// Проверяем, что числа от 1 до 10 (0 не принимается)
				if num1 < 1 || num1 > 10 || num2 < 1 || num2 > 10 {
					panic("Ошибка: Числа должны быть от 1 до 10")
				}

				switch parts[1] {
				case "+":
					fmt.Println(num1 + num2)
				case "-":
					fmt.Println(num1 - num2) // Allow negative results
				case "*":
					fmt.Println(num1 * num2)
				case "/":
					if num2 == 0 {
						panic("Ошибка: Деление на 0 невозможно")
					} else {
						fmt.Println(num1 / num2)
					}
				default:
					panic("Ошибка: Неверная математическая операция")
				}
			} else {
				panic("Ошибка: Используйте либо арабские, либо римские числа")
			}
		} else {
			panic("Ошибка: Неверный формат ввода")
		}
	}
}
